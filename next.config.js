const tinaWebpackHelpers = require("@tinacms/webpack-helpers");
const crypto = require("crypto");

module.exports = {
  webpack: (config, { buildId, dev, isServer, defaultLoaders, webpack }) => {
    if (dev) {
      tinaWebpackHelpers.aliasTinaDev(config, "../../../github/tschie/tinacms");
    }

    // Important: return the modified config
    return config;
  },
  serverRuntimeConfig: {
    // generate signing key for runtime server encryption
    signingKey: crypto.randomBytes(32).toString("base64"),
    clientSecret: process.env.GITLAB_CLIENT_SECRET,
  },
};
