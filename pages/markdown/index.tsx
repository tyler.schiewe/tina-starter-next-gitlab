import React from "react";
import Link from "next/link";
import { GetStaticProps } from "next";
import { getListMarkdownPageProps } from "../../src/utils/getListMarkdownPageProps";
import SEO from "../../src/components/seo";

export default function Markdown({ pages }) {
  return (
    <>
      <SEO title="Markdown" description="Markdown examples" />
      <h1>Markdown</h1>
      {pages.map((page) => (
        <Link key={page.fileName} href={`/markdown${page.fileName}`}>
          <a>
            <h4>{page.data.frontmatter.title}</h4>
          </a>
        </Link>
      ))}
    </>
  );
}

export const getStaticProps: GetStaticProps = async ({
  preview,
  previewData,
}) => {
  return await getListMarkdownPageProps(
    preview,
    previewData,
    "content/pages/markdown"
  );
};
