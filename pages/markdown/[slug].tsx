import React from "react";
import { GetStaticPaths, GetStaticProps } from "next";
import { usePlugin } from "tinacms";
import ReactMarkdown from "react-markdown";
import { useGitlabMarkdownForm } from "react-tinacms-gitlab";
import { useRouter } from "next/router";
import Custom404Page from "../404";
import SEO from "../../src/components/seo";
import { getMarkdownPageProps } from "../../src/utils/getMarkdownPageProps";
import { getMarkdownPagePaths } from "../../src/utils/getMarkdownPagePaths";

export default function MarkdownPage({ file }) {
  const router = useRouter();

  if (!file) {
    return <Custom404Page />;
  }

  if (router.isFallback) {
    return <div>Loading...</div>;
  }

  const [page, form] = useGitlabMarkdownForm(file, {
    label: "Markdown Page",
    fields: [
      {
        label: "Title",
        name: "frontmatter.title",
        component: "text",
        parse(value) {
          return value || "";
        },
      },
      {
        label: "Description",
        name: "frontmatter.description",
        component: "text",
        parse(value) {
          return value || "";
        },
      },
      {
        label: "Body",
        name: "markdownBody",
        component: "markdown",
        parse(value) {
          return value || "";
        },
      },
    ],
  });
  usePlugin(form);

  return (
    <>
      <SEO
        title={page.frontmatter.title}
        description={page.frontmatter.description}
      />
      <h1>{page.frontmatter.title}</h1>
      <i>{page.frontmatter.description}</i>
      <ReactMarkdown>{page.markdownBody}</ReactMarkdown>
    </>
  );
}

export const getStaticProps: GetStaticProps = async ({
  params,
  preview,
  previewData,
}) => {
  return await getMarkdownPageProps(
    preview,
    previewData,
    `content/pages/markdown/${params.slug}.md`
  );
};

export const getStaticPaths: GetStaticPaths = async () => {
  return await getMarkdownPagePaths("content/pages/markdown");
};
