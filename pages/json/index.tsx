import React from "react";
import Link from "next/link";
import { GetStaticProps } from "next";
import SEO from "../../src/components/seo";
import { getListJsonPageProps } from "../../src/utils/getListJsonPageProps";

export default function Json({ pages }) {
  return (
    <>
      <SEO title="Json" description="Json examples" />
      <h1>Json</h1>
      {pages.map((page) => (
        <Link key={page.fileName} href={`/json${page.fileName}`}>
          <a>
            <h4>{page.data.title}</h4>
          </a>
        </Link>
      ))}
    </>
  );
}

export const getStaticProps: GetStaticProps = async ({
  preview,
  previewData,
}) => {
  return await getListJsonPageProps(preview, previewData, "content/pages/json");
};
