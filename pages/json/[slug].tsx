import React from "react";
import { GetStaticPaths, GetStaticProps } from "next";
import { usePlugin } from "tinacms";
import ReactMarkdown from "react-markdown";
import { useGitlabJsonForm } from "react-tinacms-gitlab";
import { useRouter } from "next/router";
import Custom404Page from "../404";
import { getJsonPageProps } from "../../src/utils/getJsonPageProps";
import { getJsonPagePaths } from "../../src/utils/getJsonPagePaths";
import SEO from "../../src/components/seo";

export default function JsonPage({ file }) {
  const router = useRouter();

  if (!file) {
    return <Custom404Page />;
  }

  if (router.isFallback) {
    return <div>Loading...</div>;
  }

  const [page, form] = useGitlabJsonForm(file, {
    label: "Json Page",
    fields: [
      {
        label: "Title",
        name: "title",
        component: "text",
        parse(value) {
          return value || "";
        },
      },
      {
        label: "Description",
        name: "description",
        component: "text",
        parse(value) {
          return value || "";
        },
      },
      {
        label: "Body",
        name: "body",
        component: "markdown",
        parse(value) {
          return value || "";
        },
      },
      {
        label: "Image",
        name: "image",
        component: "image",
        parse: (media) => media.id,
        uploadDir: () => "/images",
      },
    ],
  });
  usePlugin(form);

  return (
    <>
      <SEO title={page.title} description={page.description} />
      <h1>{page.title}</h1>
      <i>{page.description}</i>
      <ReactMarkdown>{page.body}</ReactMarkdown>
    </>
  );
}

export const getStaticProps: GetStaticProps = async ({
  params,
  preview,
  previewData,
}) => {
  return await getJsonPageProps(
    preview,
    previewData,
    `content/pages/json/${params.slug}.json`
  );
};

export const getStaticPaths: GetStaticPaths = async () => {
  return await getJsonPagePaths("content/pages/json");
};
