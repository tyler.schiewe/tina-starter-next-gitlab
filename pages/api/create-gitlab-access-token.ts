import { createAuthHandler } from "next-tinacms-gitlab";
import getConfig from "next/config";

export default createAuthHandler(
  process.env.NEXT_PUBLIC_GITLAB_REDIRECT_URI,
  process.env.NEXT_PUBLIC_GITLAB_CLIENT_ID || "",
  getConfig().serverRuntimeConfig.clientSecret || "",
  getConfig().serverRuntimeConfig.signingKey
);
