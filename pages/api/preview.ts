import { previewHandler } from "next-tinacms-gitlab";
import getConfig from "next/config";

export default previewHandler(
  process.env.NEXT_PUBLIC_GITLAB_BASE_BRANCH,
  getConfig().serverRuntimeConfig.signingKey
);
