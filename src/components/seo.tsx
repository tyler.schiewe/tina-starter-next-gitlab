import React, { FC } from "react";
import Head from "next/head";

interface SEOProps {
  title?: string;
  description?: string;
  image?: string;
  article?: boolean;
}

const SEO: FC<SEOProps> = ({ title, description, image, article }) => {
  return (
    <Head>
      <title>{title}</title>
      <meta name="description" content={description} />
      {(article ? true : null) && <meta property="og:type" content="article" />}
      {title && <meta property="og:title" content={title} />}
      {description && <meta property="og:description" content={description} />}
    </Head>
  );
};

export default SEO;
