import { getFiles } from "next-tinacms-gitlab";
import fastGlob from "fast-glob";
import { getMarkdownPageProps } from "./getMarkdownPageProps";

export const getListMarkdownPageProps = async (
  preview: boolean,
  previewData: any,
  directory: string
) => {
  const files = preview
    ? await getFiles(
        directory,
        previewData.working_project_id,
        previewData.head_branch,
        previewData.gitlab_access_token
      )
    : await fastGlob(`${directory}/*.md`);

  const pages = await Promise.all(
    files.map(async (file) => {
      const { props } = await getMarkdownPageProps(preview, previewData, file);
      return props.file;
    })
  );

  return {
    props: {
      pages,
      preview: preview || false,
    },
  };
};
