import fastGlob from "fast-glob";

export const getJsonPagePaths = async (directory: string) => {
  const pages = await fastGlob(`${directory}/*.json`);

  const slugs = pages.map((fileName) =>
    fileName
      .replace(new RegExp(`^${escape(directory)}\/|\.json$`, "g"), "")
      .replace(/ /g, "-")
  );

  const paths = slugs.map((slug) => ({ params: { slug: slug } }));

  return {
    paths: paths,
    fallback: !process.env.NEXT_PUBLIC_STATIC,
  };
};

const escape = (string) => {
  return string.replace(/[-\/\\^$*+?.()|[\]{}]/g, "\\$&");
};
